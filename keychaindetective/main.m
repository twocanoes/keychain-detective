//
//  main.m
//  keychaindetective
//
//  Created by Timothy Perfitt on 8/20/21.
//

#import <Foundation/Foundation.h>
#include <unistd.h>
#import "TCSKeychain.h"
#import "NSData+HexString.h"

void usage(void);
void printOSStatusError(int s);
void printACLEntries(NSArray *inArray, NSString *itemName);
NSString *appNameFromRef(SecTrustedApplicationRef appRef);
void usage(){
    fprintf(stderr,"\nkeychaindetective prints and decodes access control lists from the macOS keychain and sets identity preferences.\n\n");
    fprintf(stderr,"Usage:\n");
    fprintf(stderr,"keychaindetective -l <label of private key or generic password>\n");
    fprintf(stderr,"keychaindetective -i <identity preference to set> -c <cert subject name>|-h <cert sha1 hash> [-s]\n\n");
    fprintf(stderr, "-l label\n");
    fprintf(stderr, "-i <identity preference>: Common values include com.apple.network.eap.user.identity.wlan.ssid.<SSID> or com.apple.network.eap.system.identity.wlan.ssid.<SSID> where <SSID> is the SSID of your wireless network.\n");
    fprintf(stderr, "-s: use system keychain. Requires root or sudo.\n");
    fprintf(stderr, "-c <subject>: subject name of the certificate associated with the identity. There must be exactly one identity that matches.\n");
    fprintf(stderr, "-h <hash>: sha1 hash of the certificate associated with the identity.\n");




}
int main(int argc, const char * argv[]) {
    @autoreleasepool {

        int ch;
        BOOL lflag=NO, iflag=NO, hflag=NO,sflag=NO,cflag=NO;
        lflag = 0;
        NSString *label=nil;
        NSString *identityPreferenceLabel;
        NSString *subjectName;

        NSString *identityHashString;
        while ((ch = getopt(argc, (char * const*)argv, "l:i:h:sc:")) != -1) {
            switch (ch) {
                case 'l':
                    lflag = YES;
                    label=[NSString stringWithUTF8String:optarg];
                    break;
                case 'i':
                    iflag=YES;
                    identityPreferenceLabel=[NSString stringWithUTF8String:optarg];
                    break;
                case 'h':
                    hflag=YES;
                    identityHashString=[NSString stringWithUTF8String:optarg];
                    break;
                case 'c':
                    cflag=YES;
                    subjectName=[NSString stringWithUTF8String:optarg];
                    break;

                case 's':
                    sflag=YES;
                    break;
                case '?':
                default:
                    usage();
            }
        }
        argc -= optind;
        argv += optind;


        if (lflag){
            NSArray *keys=[TCSKeychain findKeysWithLabel:label];
            if (keys.count>0){
                printf("Keys\n");
                printACLEntries(keys,@"Key");
            }
            NSArray *genericPasswords=[TCSKeychain findGenericPasswordWithLabel:label];
            if (genericPasswords.count>0){
                printf("Generic Password\n");
                printACLEntries(genericPasswords,@"Generic Passwords");
            }


        }
        else if (iflag){

            if (hflag==NO && sflag==NO){

                fprintf(stderr, "You must provide the sha1 hash of the certificate with the -h option or the certificate subject using the -s option.\n");
                exit(-1);
            }
            SecIdentityRef identity=nil;

            if (hflag){
                NSData *hashData=[NSData dataWithHexString:identityHashString];
                if (!hashData){
                    fprintf(stderr, "invalid hash\n");
                    exit(-1);
                }
                [TCSKeychain findIdentityWithSHA1Hash:hashData returnIdentity:&identity];
                if (!identity) {
                    fprintf(stderr, "no identity found with SHA1 hash of %s\n", identityHashString.UTF8String);
                    exit(-1);

                }
            }
            else if (cflag){
                identity=[TCSKeychain findIdentityWithSubject:subjectName];
                if (!identity){

                    fprintf(stderr, "no identity found or multiple identities found with subject name of %s\n", subjectName.UTF8String);
                    exit(-1);

                }
            }
            if (sflag){
                OSStatus s=SecKeychainSetPreferenceDomain(kSecPreferencesDomainSystem);
                if (s!=0){
                    printOSStatusError(s);
                    fprintf(stderr,"Verify that you are running as root. Perhaps sudo?\n");
                    exit(-1);
                }

            }

            if (!identity){
                fprintf(stderr,"No identity found\n");
                exit(-1);

            }
            OSStatus s=SecIdentitySetPreferred(identity,(CFStringRef)identityPreferenceLabel,NULL);
            if (s!=0){
                printOSStatusError(s);
                exit(-1);
            }


        }
        else {

            usage();
        }

    }
    return 0;
}

void printACLEntries(NSArray *inArray, NSString *itemName){
    [inArray enumerateObjectsUsingBlock:^(id key, NSUInteger idx, BOOL * _Nonnull stop) {
        SecAccessRef  access;
        printf("    %s %li:\n",itemName.UTF8String,idx);
        OSStatus s=SecKeychainItemCopyAccess((__bridge SecKeychainItemRef)key,&access );
        if (s!=0){
            printOSStatusError(s);
            exit(-1);
        }
        CFArrayRef aclList;
        s=SecAccessCopyACLList(access, &aclList);
        if (s!=0){
            printOSStatusError(s);
            exit(-1);
        }

        [(__bridge NSArray *)aclList enumerateObjectsUsingBlock:^(id  currACL, NSUInteger idx, BOOL * _Nonnull stop) {
            printf("        ACL %li:\n",idx);
            CFArrayRef  applicationList;
            CFStringRef   description;
            SecKeychainPromptSelector  promptSelector;

            SecACLCopyContents((SecACLRef)currACL, &applicationList, &description, &promptSelector);
            NSArray *authorizations=(NSArray *)CFBridgingRelease(SecACLCopyAuthorizations((SecACLRef)currACL));
            if (authorizations.count>0){
                printf("            Authorizations:\n");
                [authorizations enumerateObjectsUsingBlock:^(NSString *currAuth , NSUInteger idx, BOOL * _Nonnull stop) {
                    printf("                Authorization %li: %s\n",idx,currAuth.UTF8String);


                }];

            }
            else {
                printf("no authorizations\n");
            }


            NSArray *appList=(__bridge NSArray *)applicationList;
            NSString *desc=(__bridge NSString *)description;
            printf("            Description: %s\n",desc.UTF8String);

            NSData *data=[NSData dataWithHexString:desc];
            if (data){
                NSString *stringFromData=[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
                if (stringFromData && stringFromData.length>0){

                    printf("            Decoded Description:\n            ================================================\n%s\n            ================================================\n",stringFromData.UTF8String);
                }
            }
            printf("            Trusted Applications:\n");
            if (!applicationList){
                printf("                All Applications Allowed (value is nil)\n");
            }
            else if (appList.count==0){
                printf("                No Applications allowed (empty array)\n");
            }
            else {
                [appList enumerateObjectsUsingBlock:^(id  currApp, NSUInteger idx, BOOL * _Nonnull stop) {
                    NSString *name=appNameFromRef((__bridge SecTrustedApplicationRef)(currApp));
                    printf("                App %li: %s\n",idx,name.UTF8String);

                }];

            }

        }];

    }];
}
void printOSStatusError(int s){
    NSString *errorMessage=(NSString *)CFBridgingRelease(SecCopyErrorMessageString(s, NULL));
    NSLog(@"error:%@",errorMessage);

}
/*
 * _AppNameFromSecTrustedApplication attempts to pull the name of the
 * application/tool from the SecTrustedApplicationRef.
 */
NSString *appNameFromRef(SecTrustedApplicationRef appRef)
{
    CFStringRef result;
    OSStatus status;
    CFDataRef appDataRef;

    result = NULL;
    // get the data for item's application/tool
    status = SecTrustedApplicationCopyData(appRef, &appDataRef);
    if ( status == errSecSuccess ) {

        // convert it to a CFString potentially containing the path

        NSString *pathString=[[NSString alloc] initWithData:(__bridge NSData *)appDataRef encoding:NSUTF8StringEncoding];
        return pathString;
    }

    return ( @"" );
}
