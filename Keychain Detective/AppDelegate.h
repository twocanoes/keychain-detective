//
//  AppDelegate.h
//  Keychain Detective
//
//  Created by Timothy Perfitt on 8/20/21.
//

#import <Cocoa/Cocoa.h>

@interface AppDelegate : NSObject <NSApplicationDelegate>


@end

